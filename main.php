<?php

define('PREFIX', 'ci');
define('LENGTH', 10);

define('DB_HOST', 'db');
define('DB_USER', 'root');
define('DB_PASS', 'root');
define('DB_NAME', 'code_generator');

define('TABLENAME', 'codelist');
define('COLUMNNAME', 'code');

include 'CodeGenerator.php';

$generator = new CodeGenerator();
$code = $generator->getCode();

echo \sprintf("El código generado es %s\n", $code);
