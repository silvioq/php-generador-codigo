<?php

define('MAXLOOP', 100);
if (!defined('PREFIX')) {
    define('PREFIX', 'ci');
}

if (!defined('LENGTH')) {
    define('LENGTH', 10);
}

if (!defined('DB_HOST')) {
    define('DB_HOST', 'db');
}

if (!defined('DB_USER')) {
    define('DB_USER', 'root');
}

if (!defined('DB_PASS')) {
    define('DB_PASS', 'root');
}

if (!defined('DB_NAME')) {
    define('DB_NAME', 'code_generator');
}

if (!defined('TABLENAME')) {
    define('TABLENAME', 'codelist');
}

if (!defined('COLUMNNAME')) {
    define('COLUMNNAME', 'code');
}

class CodeGenerator
{
    private mysqli $connection;

    public function __construct()
    {
        $this->connection = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
        $query = "CREATE TABLE IF NOT EXISTS " . TABLENAME . " (" . COLUMNNAME . " VARCHAR(255) NOT NULL PRIMARY KEY, created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP)";
        $this->connection->query($query);
    }

    /**
     * Genera un código aleatorio
     * 
     * @param string $prefix Prefijo del código
     * @param int $length Longitud del código
     * 
     * @throws \RuntimeException Si no puede generar el código
     * 
     * @return string Código generado
     */
    public function getCode(string $prefix = PREFIX, int $length = LENGTH): string
    {
        $i = MAXLOOP;
        while($i > 0) {
            $code = $this->getRandomCode($prefix, $length);
            if(!$this->checkIfExists($code)) {
                $this->insertCode($code);
                return $code;
            }
            $i--;
        }

        throw new \RuntimeException('No se pudo generar un código seguro.');
    }

    private function getRandomCode(string $prefix, int $length): string
    {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = $prefix;
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    private function checkIfExists(string $code): bool
    {
        $query = "SELECT * FROM " . TABLENAME . " WHERE " . COLUMNNAME . " = '" . $code . "'";
        $result = $this->connection->query($query);
        $exists = $result->num_rows > 0;
        $result->close();

        return $exists;
    }

    private function insertCode(string $code): void
    {
        $query = "INSERT INTO " . TABLENAME . " (" . COLUMNNAME . ") VALUES ('" . $code . "')";
        $this->connection->query($query);
    }

}
