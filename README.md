Prueba de concepto - Generador de códigos
-----------------------------------------

La clase `Generador` genera códigos de 12 caracteres alfanuméricos, con el control sobre
una base de datos


Uso
===

Modifique en el archivo `main.php` los datos de conexión a la base de datos y ejecute:

```bash
php main.php
```

Para usarlo en su PHP:

```php
<?php

define('PREFIX', 'ci');
define('LENGTH', 10);

define('DB_HOST', 'host');
define('DB_USER', 'user');
define('DB_PASS', 'clave');
define('DB_NAME', 'nombre_db');

define('TABLENAME', 'nombre_tabla');
define('COLUMNNAME', 'nombre_columna');

$generator = new CodeGenerator();

$code = $generator->getCode();
echo \sprintf("El código generado es %s\n", $code);
``` 
En funcionamiento
=================

[![asciicast](https://asciinema.org/a/OlX5VB4wp7Y2k2nGlOnqZfKMA.svg)](https://asciinema.org/a/OlX5VB4wp7Y2k2nGlOnqZfKMA)


Test con Docker
===============

Para ejecutar la prueba de concepto con docker, ejecute:

```bash
docker-compose up -d
docker-compose run php-console main.php
```

Partes relevantes del código
============================

Este es el código que genera el código aleatorio:

```php
<?php
// CodeGenerator.php

    /**
     * Genera un código aleatorio
     * 
     * @param string $prefix Prefijo del código
     * @param int $length Longitud del código
     * 
     * @throws \RuntimeException Si no puede generar el código
     * 
     * @return string Código generado
     */
    public function getCode(string $prefix = PREFIX, int $length = LENGTH): string
    {
        $i = MAXLOOP;
        while($i > 0) {
            $code = $this->getRandomCode($prefix, $length);
            if(!$this->checkIfExists($code)) {
                return $code;
            }
            $i--;
        }

        throw new \RuntimeException('No se pudo generar un código seguro.');
    }

```